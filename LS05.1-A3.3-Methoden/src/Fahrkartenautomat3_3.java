import java.util.Scanner;

class Fahrkartenautomat {
	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);

		double zuZahlenderBetrag;
		double eingezahlterGesamtbetrag;
		double eingeworfeneM�nze;
		double r�ckgabebetrag;
		double anzahlTicket;

		System.out.print("Zu zahlender Betrag (EURO): ");
		zuZahlenderBetrag = fahrkartenBestellungErfassen();

		System.out.print("Geben sie die gew�nschte Anzahl an Tickets an:");
		anzahlTicket = fahrkartenBestellungErfassen();

		// Geldeinwurf
		// -----------
		eingezahlterGesamtbetrag = 0.0;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag * anzahlTicket) {
			System.out.printf("Noch zu zahlen: %.2f Euro",
					(zuZahlenderBetrag * anzahlTicket - eingezahlterGesamtbetrag));
			System.out.printf("\nEingabe (mind. 5Ct, h�chstens 2 Euro): ");
			eingeworfeneM�nze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneM�nze;
		}
		

		System.out.printf("Ihr Wechselgeld betr�gt: %.2f",
				fahrkartenBezahlen(eingezahlterGesamtbetrag, zuZahlenderBetrag, anzahlTicket));

		// Fahrscheinausgabe
		// -----------------
		fahrkartenAusgeben();

		// R�ckgeldberechnung und -Ausgabe
		// -------------------------------
		r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag * anzahlTicket;
		warte(2500);
		System.out.printf("Der R�ckgabebetrag in H�he von %.2f Euro wird in folgenden M�nzen ausgezahlt: \n",
				rueckgeldAusgeben(r�ckgabebetrag));
		muenzeAusgeben(r�ckgabebetrag);

		warte(1000);
		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir w�nschen Ihnen eine gute Fahrt.");
	}

	// Methoden Fahrkartenautomat Aufgabe 3.3
	public static double fahrkartenBestellungErfassen() {
		{
			Scanner myScanner = new Scanner(System.in);
			double fahrkarte = myScanner.nextDouble();
			return fahrkarte;
		}
	}

	public static double fahrkartenBezahlen(double eingezahlterGesamtbetrag, double zuZahlenderBetrag,
			double ticketAnzahl) {
		double wechselgeld = eingezahlterGesamtbetrag - zuZahlenderBetrag * ticketAnzahl;
		return wechselgeld;
	}

	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
			}
		}
		System.out.println("\n\n");
	}

	public static double rueckgeldAusgeben(double r�ckgabebetrag) {
		double wechselgeld = r�ckgabebetrag;
		return wechselgeld;
	}
	
	// warte-Methode A3.4
		public static void warte(long millisekunde) {
			try {
				Thread.sleep(millisekunde);
			} catch (InterruptedException ignored) {

			}

		}

	//Aufgabe A3.4
	public static void muenzeAusgeben(double r�ckgabebetrag) {
		if (r�ckgabebetrag > 0.0) {
			r�ckgabebetrag = Math.round(r�ckgabebetrag * 100) / 100.0;
			/*
			 * System.out.printf("Der R�ckgabebetrag in H�he von %.2f Euro ",
			 * r�ckgabebetrag, " EURO ");
			 * System.out.println("wird in folgenden M�nzen ausgezahlt:");
			 */

			while (r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
			{
				System.out.println("2 EURO");
				r�ckgabebetrag -= 2.0;
				r�ckgabebetrag = Math.round(r�ckgabebetrag * 100) / 100.0;
			}
			while (r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
			{
				System.out.println("1 EURO");
				r�ckgabebetrag -= 1.0;
				r�ckgabebetrag = Math.round(r�ckgabebetrag * 100) / 100.0;
			}
			while (r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
			{
				System.out.println("50 CENT");
				r�ckgabebetrag -= 0.5;
				r�ckgabebetrag = Math.round(r�ckgabebetrag * 100) / 100.0;
			}
			while (r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
			{
				System.out.println("20 CENT");
				r�ckgabebetrag -= 0.2;
				r�ckgabebetrag = Math.round(r�ckgabebetrag * 100) / 100.0;
			}
			while (r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
			{
				System.out.println("10 CENT");
				r�ckgabebetrag -= 0.1;
				r�ckgabebetrag = Math.round(r�ckgabebetrag * 100) / 100.0;
			}
			while (r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
			{
				System.out.println("5 CENT");
				r�ckgabebetrag -= 0.05;
				r�ckgabebetrag = Math.round(r�ckgabebetrag * 100) / 100.0;
			}
		}

	}
}