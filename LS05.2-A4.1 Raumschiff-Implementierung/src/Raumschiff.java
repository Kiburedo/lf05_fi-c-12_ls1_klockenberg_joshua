import java.util.ArrayList;
import java.util.ListIterator;
import java.util.*;

/**
 *
 * @author joshua.klockenberg
 * @since 18.05.2022
 */
public class Raumschiff 
{
	
	private String schiffsname;
	private int androidenAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int photonentorpedoAnzahl;
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();
	private ArrayList<String> broadcastKommunikator = new ArrayList<String>();

	/**
	 * Erstellt ein Objekt der Klasse Raumschiff.
	 * Dafuer werden die einzelnen Parameter in dem Konstruktor aufgefuehrt.
	 * @param schiffsname
	 * @param androidenAnzahl
	 * @param energieversorgungInProzent
	 * @param schildeInProzent
	 * @param huelleInProzent
	 * @param lebenserhaltungssystemeInProzent
	 * @param photonentorpedoAnzahl
	 */
	
	public Raumschiff(String schiffsname, int androidenAnzahl, int energieversorgungInProzent, int schildeInProzent,
			int huelleInProzent, int lebenserhaltungssystemeInProzent, int photonentorpedoAnzahl)
	{
		this.schiffsname = schiffsname;
		this.androidenAnzahl = androidenAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	
	}
	
	//get + set-Methoden
	/**
	 * Der getter fuer das Attribut schiffsname.
	 * @return String
	 */
	public String getSchiffsname() {
		return schiffsname;
	}
	/**
	 * Der setter fuer das Attribut schiffsname.
	 * @param schiffsname
	 */
	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}
	/**
	 * Der getter fuer das Attribut androidenAnzahl.
	 * @return int
	 */
	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}
	/**
	 * Der setter fuer das Attribut androidenAnzahl.
	 * @param androidenAnzahl
	 */
	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}
	/**
	 * Der getter fuer das Attribut energieversorgungInProzent
	 * @return int
	 */
	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}
	/**
	 * Der setter fuer das Attribut energieversorgungInProzent.
	 * @param engergieversorgungInProzent
	 */
	public void setEnergieversorgungInProzent(int engergieversorgungInProzent) {
		this.energieversorgungInProzent = engergieversorgungInProzent;
	}
	/**
	 * Der getter fuer das Attribut schildeInProzent.
	 * @return int
	 */
	public int getSchildeInProzent() {
		return schildeInProzent;
	}
	/**
	 * Der setter fuer das Attribut schildeInProzent.
	 * @param schildeInProzent
	 */
	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}
	/**
	 * Der getter fuer das Attribut huelleInProzent.
	 * @return int
	 */
	public int getHuelleInProzent() {
		return huelleInProzent;
	}
	/**
	 * Der setter fuer das Attribut huelleInProzent.
	 * @param huelleInProzent
	 */
	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}
	/**
	 * Der getter fuer das Attribut lebenserhaltungssystemeInProzent.
	 * @return int
	 */
	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}
	/**
	 * Der setter fuer das Attribut lebenserhaltungssystemeInProzent.
	 * @param lebenserhaltungssytemeInProzent
	 */
	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssytemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssytemeInProzent;
	}
	/**
	 * Der getter fuer das Attribut photonentorpedoAnzahl.
	 * @return
	 */
	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}
	/**
	 * Der setter fuer das Attribut photonentorpedoAnzahl
	 * @param photonentorpedoAnzahl
	 */
	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}
	
	//Weitere Methoden
	/**
	 * In dieser Methode wird eine Nachricht ausgegeben, wenn ein anderes Schiff getroffen wurde.
	 * <p>
	 * Au�erdem wird bestimmt, wie viel Schaden ein Schiff nach einem Treffer erhaelt.
	 * @param ziel
	 */
	private void treffer(Raumschiff ziel)
	{
		System.out.println(ziel.schiffsname + " wurde getroffen!");
		
		if(ziel.schildeInProzent > 50)
		{
			ziel.schildeInProzent = schildeInProzent - 50;
		}
		else if(ziel.schildeInProzent <= 50)
		{
			ziel.schildeInProzent = 0;
			if(ziel.schildeInProzent == 0)
			{
				if(ziel.energieversorgungInProzent > 50)
				{
					ziel.energieversorgungInProzent = energieversorgungInProzent - 50;
				}
				else if(ziel.energieversorgungInProzent <= 50) 
				{
					ziel.energieversorgungInProzent = 0;
				}
				if(ziel.huelleInProzent > 50)
				{
					ziel.huelleInProzent = huelleInProzent - 50;
				}
				else if(ziel.huelleInProzent <= 50)
				{
					ziel.huelleInProzent = 0;
				}
			}
		}
		if(ziel.schildeInProzent == 0 && ziel.energieversorgungInProzent == 0 && ziel.huelleInProzent == 0)
		{
			ziel.lebenserhaltungssystemeInProzent = 0;
			nachrichtAnAlle(ziel.schiffsname + ": Die Lebenserhaltungssysteme wurden vollstaendig vernichtet!");
		}
	}
	
	/**
	 * Diese Methode fuegt ein Element der ArrayList "Ladungsverzeichnis" hinzu.
	 * @param ladung
	 */
	public void addLadung(Ladung ladung)
	{
		this.ladungsverzeichnis.add(ladung);
	}
	/**
	 * Diese Methode entfernt ein Element der ArrayList "Ladungsverzeichnis".
	 * @param ladung
	 */
	
	/**
	 * Mit dieser Methode wird das Ladungsverzeichnis eines beliebigen Schiffs ausgegeben.
	 */
	public void ladungsverzeichnisAusgeben()
	{
		System.out.println(schiffsname + " Ladung: " + ladungsverzeichnis);
	}
	/**
	 * Mit dieser Methode wird der ArrayList "broadcastKommunikator" eine Nachricht hinzugefuegt, nachdem eine verschickt wurde.
	 * <p>
	 * Siehe Methode "nachrichtenAnAlle".
	 * @param broadcastKommunikator
	 */
	public void addLogbuchEintrag(String broadcastKommunikator)
	{
		this.broadcastKommunikator.add(broadcastKommunikator);
	}
	/**
	 * Mit dieser Methode wird das Logbuch eines beliebigen Schiffs ausgegeben.
	 */
	public void eintraegeLogbuchZurueckgeben()
	{
		System.out.println(schiffsname + " Logbuch: " + broadcastKommunikator);
	}
	/**
	 * Mit dieser Methode wird mit einem beliebigen Schiff ein Photonentorpedo auf ein beliebiges anderes Schiff geschossen.
	 * @param ziel
	 */
	public void photonentorpedoSchiessen(Raumschiff ziel)
	{
		if(photonentorpedoAnzahl == 0)
		{
			nachrichtAnAlle(schiffsname + ": -=*Click*=-");
		}
		if(photonentorpedoAnzahl >= 1)
		{
			photonentorpedoAnzahl = photonentorpedoAnzahl - 1;
			nachrichtAnAlle(schiffsname + ": Photonentorpedo abgeschossen");
			treffer(ziel);
		}
		

	}
	/**
	 * Mit dieser Methode wird eine Phaserkanonen eines beliebigen Schiffes auf ein anderes beliebiges Schiff geschossen.
	 * @param ziel
	 */
	public void phaserkanoneSchiessen(Raumschiff ziel)
	{
		if(energieversorgungInProzent < 50)
		{
			nachrichtAnAlle(schiffsname + ": -=*Click*=-");
		}
		if(energieversorgungInProzent >= 50)
		{
			energieversorgungInProzent = energieversorgungInProzent - 50;
			nachrichtAnAlle(schiffsname + ": Phaserkanonen abgeschossen");
		}
		treffer(ziel);
	}

	public void photonentorpedosLaden(Ladung ladung, Raumschiff raumschiff)
		{
			if(ladung.getMenge() == 0)
			{
				nachrichtAnAlle(raumschiff.schiffsname + ": Keine Photonentorpedos gefunden!");
			}
			raumschiff.photonentorpedoAnzahl = raumschiff.photonentorpedoAnzahl + ladung.getMenge();
			ladung.setMenge(ladung.getMenge() - ladung.getMenge());
			System.out.println(schiffsname + ": " + ladung.getMenge() + " Photonentorpedos wurden nachgeladen.");
		}
	public void reparaturDurchfuehren(Raumschiff raumschiff)
	{
		int anzahlDroiden;
		anzahlDroiden = raumschiff.androidenAnzahl;
		int counter = 0;
		int min = 1;
		int max = 100;

		Random random = new Random();

		int value = random.nextInt(max + min) + min;
		
		
		if(anzahlDroiden > raumschiff.androidenAnzahl)
		{
			anzahlDroiden = raumschiff.androidenAnzahl;
		}
		
		if(raumschiff.schildeInProzent > 0)
		{
			counter = counter + 1;
		}
		else
		{
		}
		if(raumschiff.energieversorgungInProzent > 0)
		{
			counter = counter + 1;
		}
		else
		{
		}
		if(raumschiff.huelleInProzent > 0)
		{
			counter = counter + 1;
		}
		else
		{
		}
		if(counter >= 1)
		{
			raumschiff.schildeInProzent = (value*anzahlDroiden)/counter;
			raumschiff.energieversorgungInProzent = (value*anzahlDroiden)/counter;
			raumschiff.huelleInProzent = (value*anzahlDroiden)/counter;
		}
		else if(counter == 0)
		{
			nachrichtAnAlle("Reparater bei: " + schiffsname + " nicht m�glich, da das Schiff zerst�rt wurde.");
		}
	}
	/**
	 * Mit dieser Methode werden alle Parameter eines beliebigen Schiffes ausgegeben.
	 */
	public void zustandRaumschiff()
	{
		System.out.println("Name: " + this.schiffsname);
		System.out.println("Androide-Anzahl: " + this.androidenAnzahl);
		System.out.println("Energieversorung in Prozent: " + this.energieversorgungInProzent);
		System.out.println("Schilde in Prozent: " + this.schildeInProzent);
		System.out.println("Huelle in Prozent: " + this.huelleInProzent);
		System.out.println("Lebenserhaltungssysteme in Prozent: " + this.lebenserhaltungssystemeInProzent);
		System.out.println("Anzahl an Photonentorpedos: " + this.photonentorpedoAnzahl);
	}
	public void ladungsverzeichnisAufrauemen(Ladung ladung, Raumschiff raumschiff)
	{
		this.ladungsverzeichnis.remove(ladung);
	}
	/**
	 * Mit dieser Methode wird eine Nachricht an Alle innerhalb des Raumschiffes gesendet.
	 * <p>
	 * Zusaetzlich wird die gesendete Nachricht im Logbuch vermerkt, um sie spaeter evtl. wieder ausgeben zu koennen.
	 * @param message
	 */
	public void nachrichtAnAlle(String message)
	{
		System.out.println(message);
		addLogbuchEintrag(message);
	}
}
