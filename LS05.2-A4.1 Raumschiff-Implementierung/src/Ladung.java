/**
 * 
 * @author joshua.klockenberg
 *
 */
public class Ladung 
{
	
	private String bezeichnung;
	private int menge;
	
	/**
	 * Erstellt ein Objekt der Klasse Ladung.
	 * Dafuer werden die einzelnen Parameter in dem Konstruktor aufgefuehrt.
	 * @param bezeichnung
	 * @param menge
	 */
	public Ladung(String bezeichnung, int menge)
	{
		this.bezeichnung = bezeichnung;
		this.menge = menge;
	}
	
	//get + set-Methoden
	
	/**
	 * Der getter fuer das Attribut bezeichnung.
	 * @return String
	 */
	public String getBezeichnung() {
		return bezeichnung;
	}
	/**
	 * Der setter fuer das Attribut bezeichnung.
	 * @param bezeichnung
	 */
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}
	/**
	 * Der getter fuer das Attribut menge.
	 * @return int
	 */
	public int getMenge() {
		return menge;
	}
	/**
	 * Der setter fuer das Attribut menge.
	 * @param menge
	 */
	public void setMenge(int menge) {
		this.menge = menge;
	}
	
	@Override
	public String toString()
	{
		return bezeichnung + " (" + menge + ")";
	}
	
}
