import java.util.Scanner;

public class TestArrays {

	public static void main(String[] args) {
		
		int[] array = new int[5];
		
		for(int i = 0; i <= 4; i++)
		{
			Scanner myScanner = new Scanner(System.in);
			System.out.println("Geben sie eine Zahl ein.");
			array[i] = myScanner.nextInt();
		}
		System.out.print("Ihr Array ist: ");
		
		for(int j = 0; j <= 4; j++)
		{
			System.out.print(array[j]);
			System.out.print(", ");
		}
		System.out.printf("\n \n");
		System.out.println("Ihr Array sieht umgekehrt so aus: ");
		
		for(int i = 4; i >= 0; i--)
		{
			System.out.println(array[i]);
		}
	}
}