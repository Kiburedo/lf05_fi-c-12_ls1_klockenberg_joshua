import java.util.Arrays;

public class Array_A4 {

	public static void main(String[] args) {
		
		int[] lotto = {3, 7, 12, 18, 37, 42};
	
		System.out.print("Ihr Lottozahlen sind: [ ");
		for(int j = 0; j <= 5; j++)
		{
			System.out.print(lotto[j]);
			System.out.print("  ");
		}
		System.out.println("]\n");
		
		boolean val = false;
		for(int i = 0; i <= 5; i++)
		{			
			
			if(lotto[i] == 12)
			{
				val = true;
			}
			
			if(i == 5 && val == true)
			{
				System.out.println("Die Zahl 12 ist in der Ziehung enthalten");
			}
		}

		boolean val1 = false;
		for(int i = 0; i <= 5; i++)
		{			
			
			if(lotto[i] != 13)
			{
				val1 = true;
			}
			
			if(i == 5 && val1 == true)
			{
				System.out.println("Die Zahl 13 ist in der Ziehung nicht enthalten");
			}
		}
	}
}

