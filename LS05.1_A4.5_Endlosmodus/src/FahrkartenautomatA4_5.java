import java.util.Scanner;

public class FahrkartenautomatA4_5 {
	
	public static void main(String[] args) {
				
				while (true) // F�r den Endlosmodus des Fahrkartenautomats (Aufgabe 4.5 Unteraufgabe 2)
				{
				Scanner tastatur = new Scanner(System.in);

				double zuZahlenderBetrag;
				double eingezahlterGesamtbetrag;
				double eingeworfeneM�nze;
				double r�ckgabebetrag;
				double anzahlTicket;

				
				System.out.println("W�hlen Sie sich ein Ticket f�r den Bereich Berlin AB aus:");
				System.out.print("Einzelfahrschein Regeltarif AB [2,90 EUR] (1) \n" + "Tageskarte Regeltarif AB [8,60] (2) \n" + "Kleingruppen-Tageskarte Regeltarif AB [23,50] (3)");
				zuZahlenderBetrag = fahrkartenBestellungErfassen();

				System.out.print("Geben sie die gew�nschte Anzahl an Tickets an:");
				anzahlTicket = ticketAnzahlErfassen();

				// Geldeinwurf
				// -----------
				eingezahlterGesamtbetrag = 0.0;
				while (eingezahlterGesamtbetrag < zuZahlenderBetrag * anzahlTicket) {
					System.out.printf("Noch zu zahlen: %.2f Euro",
							(zuZahlenderBetrag * anzahlTicket - eingezahlterGesamtbetrag));
					System.out.printf("\nEingabe (mind. 5Ct, h�chstens 2 Euro): ");
					eingeworfeneM�nze = tastatur.nextDouble();
					eingezahlterGesamtbetrag += eingeworfeneM�nze;
				}
				

				System.out.printf("Ihr Wechselgeld betr�gt: %.2f",
						fahrkartenBezahlen(eingezahlterGesamtbetrag, zuZahlenderBetrag, anzahlTicket));

				// Fahrscheinausgabe
				// -----------------
				fahrkartenAusgeben();

				// R�ckgeldberechnung und -Ausgabe
				// -------------------------------
				r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag * anzahlTicket;
				warte(2000);
				System.out.printf("Der R�ckgabebetrag in H�he von %.2f Euro wird in folgenden M�nzen ausgezahlt: \n",
						rueckgeldAusgeben(r�ckgabebetrag));
				muenzeAusgeben(r�ckgabebetrag);

				warte(1500);
				System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
						+ "Wir w�nschen Ihnen eine gute Fahrt.");
				warte(750); //Kleine Pause zwischen Ende der Bestellung und dem Starten einer Bestellung, des Fahrkartenautomats
				System.out.println(); // Leere Zeile zwischen Ende alter Bestellung und dem Beginn der neuen Bestellung (Meiner Meinung nach einfacher anzuschauen)
			}
	}
			// Methode Aufgabe 4.5 --> unterschiedliche Tickets (Unteraufgabe 3)
			public static double fahrkartenBestellungErfassen() 
				{
					Scanner myScanner = new Scanner(System.in);
					double preis;
					int ticketArt;
					boolean ticket�berpr�fung = true;
					
					do 
					{
						ticketArt = myScanner.nextInt();
						System.out.println("Ihre Wahl ist das Ticket: " + ticketArt);
						if (ticketArt > 0 && ticketArt < 4)
						{
							ticket�berpr�fung = false;
						}
						else
						{
							System.out.println(">>falsche Eingabe<<");
						}
					}	
					while(ticket�berpr�fung);
					
					if(ticketArt == 1)
					{
						preis = 2.90;
						return preis;
					}
					if(ticketArt == 2)
					{
						preis = 8.60;
						return preis;
					}
					if(ticketArt == 3)
					{
						preis = 23.50;
						return preis;
					}
					
					return ticketArt;	
				}
				
		
			//Begrenzte Tickets --> Methode (Aufgabe 4.2)
			public static double ticketAnzahlErfassen ()
			{
				Scanner myScanner = new Scanner(System.in);
				double fahrkarte = myScanner.nextDouble();
				if(fahrkarte >= 1 && fahrkarte <= 10)
				{
					
				}
				else
				{
					System.out.println("Die Anzahl " + fahrkarte + " wird nicht zugelassen. Der Wert wird auf '1' Fahrkarte gesetzt!");
					fahrkarte = 1;
				}
				return fahrkarte;
			}

			// warte-Methode A3.4
			public static void warte(long millisekunde) {
				try {
					Thread.sleep(millisekunde);
				} catch (InterruptedException ignored) {

				}

			}

			public static double fahrkartenBezahlen(double eingezahlterGesamtbetrag, double zuZahlenderBetrag,
					double ticketAnzahl) {
				double wechselgeld = eingezahlterGesamtbetrag - zuZahlenderBetrag * ticketAnzahl;
				return wechselgeld;
			}

			public static void fahrkartenAusgeben() {
				System.out.println("\nFahrschein wird ausgegeben");
				for (int i = 0; i < 8; i++) {
					System.out.print("=");
					try {
						Thread.sleep(250);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				System.out.println("\n\n");
			}

			public static double rueckgeldAusgeben(double r�ckgabebetrag) {
				double wechselgeld = r�ckgabebetrag;
				return wechselgeld;
			}

			//Aufgabe A3.4
			public static void muenzeAusgeben(double r�ckgabebetrag) {
				if (r�ckgabebetrag > 0.0) {
					r�ckgabebetrag = Math.round(r�ckgabebetrag * 100) / 100.0;
					/*
					 * System.out.printf("Der R�ckgabebetrag in H�he von %.2f Euro ",
					 * r�ckgabebetrag, " EURO ");
					 * System.out.println("wird in folgenden M�nzen ausgezahlt:");
					 */

					while (r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
					{
						System.out.println("2 EURO");
						r�ckgabebetrag -= 2.0;
						r�ckgabebetrag = Math.round(r�ckgabebetrag * 100) / 100.0;
					}
					while (r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
					{
						System.out.println("1 EURO");
						r�ckgabebetrag -= 1.0;
						r�ckgabebetrag = Math.round(r�ckgabebetrag * 100) / 100.0;
					}
					while (r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
					{
						System.out.println("50 CENT");
						r�ckgabebetrag -= 0.5;
						r�ckgabebetrag = Math.round(r�ckgabebetrag * 100) / 100.0;
					}
					while (r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
					{
						System.out.println("20 CENT");
						r�ckgabebetrag -= 0.2;
						r�ckgabebetrag = Math.round(r�ckgabebetrag * 100) / 100.0;
					}
					while (r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
					{
						System.out.println("10 CENT");
						r�ckgabebetrag -= 0.1;
						r�ckgabebetrag = Math.round(r�ckgabebetrag * 100) / 100.0;
					}
					while (r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
					{
						System.out.println("5 CENT");
						r�ckgabebetrag -= 0.05;
						r�ckgabebetrag = Math.round(r�ckgabebetrag * 100) / 100.0;
					}
				}

			}
		}
