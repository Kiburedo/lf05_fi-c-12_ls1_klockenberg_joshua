import java.util.Scanner;

public class A4_1_3_RoemischeZahlen {

	public static void main(String[] args) {
		System.out.println("Geben sie mir bitte einen der folgenden Buchstaben an.");
		warte(1000);
		System.out.println("I, V, X, L, C, D, M");
		Scanner myScanner = new Scanner(System.in);

		char buchstabe = myScanner.next().charAt(0);

		if (buchstabe == 'I') {
			System.out.println("Die r�mische Zahl f�r dieses Zeichen lautet: 1");
		}

		if (buchstabe == 'V') {
			System.out.println("Die r�mische Zahl f�r dieses Zeichen lautet: 5");
		}

		if (buchstabe == 'X') {
			System.out.println("Die r�mische Zahl f�r dieses Zeichen lautet: 10");
		}

		if (buchstabe == 'L') {
			System.out.println("Die r�mische Zahl f�r dieses Zeichen lautet: 50");
		}

		if (buchstabe == 'C') {
			System.out.println("Die r�mische Zahl f�r dieses Zeichen lautet: 100");
		}

		if (buchstabe == 'D') {
			System.out.println("Die r�mische Zahl f�r dieses Zeichen lautet: 500");
		}

		if (buchstabe == 'M') {
			System.out.println("Die r�mische Zahl f�r dieses Zeichen lautet: 1000");
		}

		if(buchstabe != 'I' && buchstabe != 'V' && buchstabe != 'X' && buchstabe != 'L' && buchstabe != 'C' && buchstabe != 'D' && buchstabe != 'M')
		{
			System.out.println("Dieser Buchstabe ist keine r�mische Zahl!");
		}
	}
	
	//warte Methode
	public static void warte(long millisekunde) {
		try {
			Thread.sleep(millisekunde);
		} catch (InterruptedException ignored) {

		}

	}
}
