import java.util.Scanner;

public class A4_1_4_Rabattsystem
{
	public static void main(String[] args)
	{
		Scanner myScanner = new Scanner(System.in);
		double bestellwert = myScanner.nextDouble();
		
		if(bestellwert <= 100 && bestellwert > 0)
		{
			System.out.println("Herzlichen Gl�ckwunsch! Sie erhalten 10% Rabatt!");
			double zwischenbetrag = bestellwert + (bestellwert * 0.19);
			double gesamtbetrag = zwischenbetrag - (zwischenbetrag *0.1);
			System.out.printf("Ihr Endbetrag inklusive der mwst liegt bei: %.2f �", gesamtbetrag);
		}
		else if(bestellwert > 500)
		{
			System.out.println("Herlichen Gl�ckwunsch! Sie erhalten 20% Rabatt!");
			double zwischenbetrag = bestellwert + (bestellwert * 0.19);
			double gesamtbetrag = zwischenbetrag - (zwischenbetrag * 0.2);
			System.out.printf("Ihr Endbetrag inklusive der mwst liegt bei: %.2f�", gesamtbetrag);
		}
	}
}