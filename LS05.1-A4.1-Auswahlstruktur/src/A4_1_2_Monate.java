import java.util.Scanner;

public class A4_1_2_Monate {

	public static void main(String[] args) {
		// Aufgabe 2

		System.out.println("Geben Sie eine Zahl von 1-12 ein.");
		Scanner myScanner = new Scanner(System.in);
		
		int monat = myScanner.nextInt();

		if (monat == 1) {
			System.out.println("Ihr Monat ist: Januar.");
		}

		if (monat == 2) {
			System.out.println("Ihr Monat ist: Februar.");
		}

		if (monat == 3) {
			System.out.println("Ihr Monat ist: M�rz.");
		}

		if (monat == 4) {
			System.out.println("Ihr Monat ist: April.");
		}

		if (monat == 5) {
			System.out.println("Ihr Monat ist: Mai.");
		}

		if (monat == 6) {
			System.out.println("Ihr Monat ist: Juni.");
		}

		if (monat == 7) {
			System.out.println("Ihr Monat ist: Juli.");
		}

		if (monat == 8) {
			System.out.println("Ihr Monat ist: August.");
		}

		if (monat == 9) {
			System.out.println("Ihr Monat ist: September.");
		}

		if (monat == 10) {
			System.out.println("Ihr Monat ist: Oktober.");
		}

		if (monat == 11) {
			System.out.println("Ihr Monat ist: November.");
		}

		if (monat == 12) {
			System.out.println("Ihr Monat ist: Dezember.");
		}

		if (monat <= 0 || monat > 12) {
			System.out.println("Diesen Monat gibt es nicht.");
		}
	}
}