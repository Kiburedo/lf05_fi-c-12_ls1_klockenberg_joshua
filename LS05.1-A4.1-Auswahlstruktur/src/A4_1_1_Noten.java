import java.util.Scanner;

public class A4_1_1_Noten {

	public static void main(String[] args) {

		System.out.println("Geben Sie eine Note von 1-6 ein.");
		Scanner myScanner = new Scanner(System.in);
		int note = myScanner.nextInt();

		if (note == 1) {
			System.out.println("Die sprachliche Umschreibung f�r: " + note + " ist: 'sehr gut'.");
		}

		if (note == 2) {
			System.out.println("Die sprachliche Umschreibung f�r: " + note + " ist: 'gut'.");
		}

		if (note == 3) {
			System.out.println("Die sprachliche Umschreibung f�r: " + note + " ist: 'befriedigend'.");
		}

		if (note == 4) {
			System.out.println("Die sprachliche Umschreibung f�r: " + note + " ist: 'ausreichend'.");
		}

		if (note == 5) {
			System.out.println("Die sprachliche Umschreibung f�r: " + note + " ist: 'mangelhaft'.");
		}

		if (note == 6) {
			System.out.println("Die sprachliche Umschreibung f�r: " + note + " ist: 'ungen�gend'.");
		}

		if (note < 0 || note > 6) {
			System.out.println("Es gibt keine Note mit dem Wert: " + note + ".");
		}
	}
}