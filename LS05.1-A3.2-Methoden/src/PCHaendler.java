import java.util.Scanner;

public class PCHaendler

{

	public static String liesString() 
	{
		Scanner myScanner = new Scanner(System.in);
		String artikelMethode = myScanner.next();
		return artikelMethode;

	}

	public static int liesInt() 
	{
		Scanner myScanner = new Scanner(System.in);
		int anzahlMethode = myScanner.nextInt();
		return anzahlMethode;

	}

	public static double liesDouble() 
	{
		Scanner myScanner = new Scanner(System.in);
		double preisMethode = myScanner.nextDouble();
		return preisMethode;

	}

	public static double berechneGesamtnettopreis(int anzahl, double nettopreis) 
	{
		return (anzahl * nettopreis);
	}

	public static double berechneGesamtbruttopreis(double nettogesamtpreis, double mwst) 
	{
		return (nettogesamtpreis * (1 + mwst / 100));
	}

	public static void main(String[] args) 
	{
		Scanner myScanner = new Scanner(System.in);

		// Benutzereingaben lesen
		System.out.println("Was m�chten Sie bestellen?");
		String artikel = liesString();

		System.out.println("Geben Sie die Anzahl ein:");
		int anzahl = liesInt();

		System.out.println("Geben Sie den Nettopreis ein:");
		double nettopreis = liesDouble();

		System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		double mwst = liesDouble();

		// Verarbeiten
		double nettogesamtpreis = berechneGesamtnettopreis(anzahl, nettopreis);
		double bruttogesamtpreis = berechneGesamtbruttopreis(nettogesamtpreis, mwst);

		// Ausgeben
		rechnungausgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);
	}

	public static void rechnungausgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis, double mwst) 
	{
				System.out.println("\tRechnung");
				System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
				System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
	}
}